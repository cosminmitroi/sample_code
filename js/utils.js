/**
 * Created by cosmin.mitroi on 7/10/14.
 */

/**
 *
 * @param {type} $locationProvider
 * @returns {undefined}
 */
function locationProviderSetConfig(locationProvider) {
    var ua = navigator.userAgent;
    if (ua.indexOf('MSIE 9') > 0) {
        locationProvider.html5Mode(false).hashPrefix("!");
        var hash = location.hash;
        if (hash.indexOf('#!/') > -1) {
            window.location.href = hash.replace(/#!/g, '');
            window.location.href.substr(0, window.location.href.indexOf('#!'));
        }
    } else {
        locationProvider.html5Mode(true).hashPrefix('!');

    }
    //this is used to not affcet the page change action 
    $("a").filter(":not([target])").attr('target', '_self');
}


/**
 * Copies the behaviour of strpos from PHP
 *
 * @param needle
 * @param offset
 * @return string
 */
String.prototype.strpos = function (needle, offset) {
    var i = (this + '').indexOf(needle, (offset || 0));
    return i === -1 ? false : i;
};


/**
 * Copies the behaviour of ucfirst from PHP
 *
 * @param str
 * @returns {string}
 */
function ucfirst(str) {
    str += '';
    var f = str.charAt(0).toUpperCase();
    return f + str.substr(1);
}

/**
 * Clear empty values from an object, recursively
 *
 * @param list
 * @returns {*}
 */
var clearObjectEmptyValues = function (list) {
    var tmp = list;

    if (list && typeof (list) === 'object') {
        $.each(list, function (key, value) {

            if (value === '' || value === null) {
                delete tmp[key];
            }

            if (value && typeof (value) === 'object') {
                if (value.length == 0 || value[0] === '') {
                    delete tmp[key];
                } else {
                    tmp[key] = clearObjectEmptyValues(value);
                }
            }
        });
    }

    return tmp;
};

/**
 *
 * @param dayScope
 * @param montScope
 * @param yearScope
 * @TODO - check day
 * @todo - add month name and translations
 * @todo - check year in the future
 */
function dateDropdownValues(dayScope, montScope, yearScope) {
    for (var d = 1; d <= 31; d++) {
        dayScope.push({
            "id": d,
            "name": d
        });
    }
    for (var m = 1; m <= 12; m++) {
        montScope.push({
            "id": m,
            "name": m
        });
    }
    for (var y = new Date().getFullYear() - 14; y >= 1940; y--) {
        yearScope.push({
            "id": y,
            "name": y
        });
    }
}