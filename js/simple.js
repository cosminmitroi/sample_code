/**
 *
 * @author: Cosmin Mitroi
 */
$(function () {

    $('#welcome-message').on('click', function (e) {
        e.preventDefault()
        $(this).fadeOut(250, function () {
            $(this).remove()
            $('#email').focus()
        })
    });

    $('body').on('submit', '.signup-form', function (e) {
        e.preventDefault();

        var $this = $(this),
            data = $this.serializeArray();

        $('.error-label', $this)
            .closest('.field').removeClass('error').end()
            .remove();

        $.post('/users/registerAjax', data, function (response) {

            // signup successful
            if ($.isEmptyObject(response.errors) || response.errors.isLogged) {

                // trigger Signup event
                if (window._gaq && $this.is('.signup-form')) {
                    _gaq.push(['_trackEvent', 'Site', 'Signup']);
                }

                window.location.href = response.redirect || '/';

                return;
            }

            if (!$.isEmptyObject(response.errors)) {
                error = '';
                var focused = false;
                $('.field', $this).removeClass('error');
                $.each(response.errors, function (key, errorMessage) {
                    var errorField = $('[name="' + key + '"]:not(:hidden)', $this);
                    var errorFieldId = errorField.attr('id');
                    errorField.closest('.field')
                        .addClass('error')
                        .append('<label class="error-label" for="' + errorFieldId + '">' + errorMessage + '</label>');

                    if (focused) return;
                    errorField.focus();
                    focused = true;
                });
            }

            // no go
            $this.find('.extra')
                .addClass('inv')
                .end()
                .find('.extra.form-errors').html(error)
                .removeClass('inv');

        });

    }); // login & signup request

});


function validEmail(email) {
    return /[a-zA-Z0-9.!#$%&'*+-/=?\^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*/.test(email);
}
