/*
 *
 * @author: Cosmin Mitroi
 */
app.directive('notification', function ($timeout) {
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        scope: {
            notificationShow: '='
        },
        link: function (scope, element, attrs) {
            scope.text = attrs.text;
            scope.show = false;
            var firstCall = true;

            scope.$watch('notificationShow', function () {
                if (firstCall) {
                    firstCall = false;
                    return;
                }

                if (scope.notificationShow) {
                    scope.show = true;
                } else {
                    $timeout(function () {
                        scope.show = false;
                    }, 5000);
                }

            });

        },
        template: "<div class='alert alert-success alert-dismissable' ng-show='show'>" +
        "<div>{{text}}</div>" +
        "</div>"
    }
});

app.directive('uiToggle', [function () {
    return function (scope, elm, attrs) {
        scope.$watch(attrs.uiToggle, function (newVal, oldVal) {
            if (newVal) {
                elm.removeClass('ui-hide').addClass('ui-show');
            } else {
                elm.removeClass('ui-show').addClass('ui-hide');
            }
        });
    };
}]);

//Password strength validation rules
app.directive('checkStrength', function () {

    return {
        replace: false,
        restrict: 'EACM',
        scope: {
            password: '='
        },
        link: function (scope, iElement, iAttrs) {

            var strength = {
                colors: ['#F00', '#F90', '#FF0', '#9F0', '#0F0'],
                mesureStrength: function (p) {

                    var _force = 0;
                    var _regex = /[!@#$%^&*()\-_=+{};:,<.>[\]]/g;

                    var _lowerLetters = /[a-z]+/.test(p);
                    var _upperLetters = /[A-Z]+/.test(p);
                    var _numbers = /[0-9]+/.test(p);
                    var _symbols = _regex.test(p);

                    var _flags = [_lowerLetters, _upperLetters, _numbers, _symbols];
                    var _passedMatches = $.grep(_flags, function (el) {
                        return el === true;
                    }).length;

                    _force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
                    _force += _passedMatches * 10;

                    // penalty (short password)
                    _force = (p.length < 6) ? Math.min(_force, 10) : _force;

                    if (_passedMatches == 1) {
                        _force = Math.min(_force, 10);
                    }

                    if (_passedMatches == 2) {
                        _force = (p.length < 16 || !(_lowerLetters && _upperLetters)) ? Math.min(_force, 30) : _force;
                    }

                    if (_passedMatches == 3) {
                        _force = (p.length < 9 || !(_lowerLetters && _upperLetters && (_numbers || _symbols))) ? Math.min(_force, 40) : _force;
                    }

                    return _force;

                },
                getColor: function (s) {
                    var idx = 0;
                    if (s <= 10) {
                        idx = 0;
                    }
                    else if (s <= 20) {
                        idx = 1;
                    }
                    else if (s <= 30) {
                        idx = 2;
                    }
                    else if (s <= 40) {
                        idx = 3;
                    }
                    else {
                        idx = 4;
                    }

                    return {idx: idx + 1, col: this.colors[idx]};

                }
            };

            scope.$watch('password', function () {
                if (scope.password === '' || typeof scope.password == 'undefined') {
                    iElement.css({"display": "none"});
                    iElement.removeAttr('data-force');
                } else {
                    if (typeof scope.password != 'undefined') {
                        var c = strength.getColor(strength.mesureStrength(scope.password));
                        iElement.attr('data-force', c.idx);
                        iElement.css({"display": "inline"});
                        iElement.children('li')
                            .css({"background": "#DDD"})
                            .slice(0, c.idx)
                            .css({"background": c.col});
                    }

                }
            });

        },
        template: '<li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li><li class="point"></li>'
    };

});