/**
 *
 * @author: Cosmin Mitroi
 */
(function ($) {
    var tracker = window._gaq;
    $(function () {

        var body = $('body');

    });// DOMContentLoaded


    $('body').on('click', '.act-list .act', function (e) {
        var label = $(this).text();
        if (tracker) {
            _gaq.push(['_trackEvent', 'Site', 'Login/Signup', label]);
        }
    });
    $('body').on('click', '.btn-create', function (e) {
        var label = $(this).text();
        if (tracker) {
            _gaq.push(['_trackEvent', 'Site', 'Create button', label]);
        }
    });

    $('body').on('click', '.act-login', function (e) {
        // allow ctrl-click
        if (e.ctrlKey || e.metaKey) {
            return true;
        }

        if (showForm($('#login-form'))) {
            e.preventDefault();
        }

    });

    $('body').on('click', '.act-signup', function (e) {
        // allow ctrl-click
        if (e.ctrlKey || e.metaKey) {
            return true;
        }

        if (showForm($('#signup-form'))) {
            e.preventDefault();
        }

    });

    $('body').on('submit', '#signup-form, #login-form', function (e) {
        e.preventDefault();

        var $this = $(this),
            data = $this.serialize(),
            error = 'Ouch!<br>You need to review the form. Please try again.';

        $.post($this.attr('action'), data, function (response) {

            // signup successful
            if ($.isEmptyObject(response.errors) || response.errors.isLogged) {

                // trigger Signup event
                if (window._gaq && $this.is('#signup-form')) {
                    _gaq.push(['_trackEvent', 'Site', 'Signup']);
                }
                else if (response.redirect) {
                    window.location.href = response.redirect;
                }
                else {
                    window.location.reload();
                }
                return;
            }

            if (!$.isEmptyObject(response.errors)) {
                error = '';
                var focused = false;
                $('.field', $this).removeClass('error');
                $.each(response.errors, function (key, errorMessage) {
                    var errorField = $('[name="' + key + '"]', $this);

                    if (error != '') error += '<br>';
                    error += errorMessage;

                    errorField.closest('.field').addClass('error')

                    if (focused) return;
                    errorField.focus();
                    focused = true;
                });
            }

            // no go
            $this.find('.extra')
                .addClass('inv')
                .end()
                .find('.extra.form-errors').html(error)
                .removeClass('inv');

        });

    }); // login & signup request


    $('body').on('keyup', function (e) {
        var keyCode = e.keyCode || e.which;

        // on escape
        if (keyCode == 27) {
            $('.overlay-form-close:visible:first').click();
        }
    });


    $('body').on('click', '.overlay-form .show-form', function (e) {
        e.preventDefault();

        var t = $(this),
            href = t.data('form') || t.attr('href');

        if (href.length < 2 || href.indexOf('#') != 0) return;
        if (!$(href).length) return;

        $('.overlay-form').addClass('inv');
        $(href).removeClass('inv')
            .find('.autofocus').eq(0).trigger('focus').end().end()
            .find('.extra').addClass('inv').eq(0).removeClass('inv').end().end()
            .appendTo($('#overlay'));
    });

    $('body').on('submit', '.overlay-form#login-recover', function (e) {
        e.preventDefault();

        var $this = $(this);
        data = $this.serialize();

        $.post($this.attr('action'), data, function (response) {
            if (response) {
                $('.overlay-form').addClass('inv');
                $('#login-form').removeClass('inv')
                    .find('.extra').addClass('inv')
                    .filter('.recovery-email-sent').removeClass('inv').find('.autofocus').eq(0).trigger('focus');
            }
        });
    });

    $('body').on('click', '.overlay-form .extra-close', function (e) {
        e.preventDefault();
        $(this).parents('.extra').eq(0).addClass('inv').siblings('.extra').first().removeClass('inv');
    });


    // make select elements pretty
    $.fn.selectmenu && $('select').selectmenu();

    $('.field.url .form-field')
        .on('focus', function (e) {
            var t = $(this);
            if (this.value.indexOf('http://') == -1) {
                this.value = 'http://' + this.value;
            }
        })
        .on('blur', function (e) {
            var t = $(this);
            if (this.value === 'http://') {
                this.value = '';
            }
        });

    if (!('placeholder' in document.createElement('input'))) {
        $.getScript('/assets/js/plugins/jquery.placeholder.min.js', function () {
            $('input, textarea').placeholder();
        });
    }

    $('#welcome-message').on('click', function (e) {
        $(this).fadeOut(250, function () {
            $(this).remove()
        })
    });


    var $header = $('.layout-head');

    //add bg on header if scrollTop <= 5
    $(window).scroll(function (e) {
        toggleHeader($header);
    });
    $(window).on('load', function (e) {
        toggleHeader($header);
    });

    //more-nav button
    $('.nav-more').on('click', function (e) {
        e.preventDefault();

        var t = $(this);
        t.toggleClass('nav-small');
        t.parent().siblings('.nav-hide').toggleClass('hide');
    });


}(jQuery));

function toggleHeader($header) {
    var top = window.pageYOffset || document.body.scrollTop;

    if (top <= 5) {
        $header.removeClass('bg-head').addClass('bg-gradient');
    } else {
        $header.removeClass('bg-gradient').addClass('bg-head');
    }
}

function showForm(whichForm) {

    if (!whichForm.length) return false;

    !$('#overlay').length && $('body').append('<div id="overlay"/>');
    whichForm.appendTo('#overlay');

    whichForm.removeClass('inv').removeClass('publish-form');
    whichForm.find('.autofocus').eq(0).trigger('focus');

    return true;

}


function showLogin(sessionExpired) {
    $form = $('#login-form');
    showForm($form);

    if (sessionExpired) {
        $form.find('.fields .field').removeClass('error').end()
            .find('.extra').addClass('inv').end()
            .find('.session-expired').removeClass('inv');
    }
}

function showRegister() {
    showForm($('#signup-form'));
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}


function hideSelfHidingNotices() {
    $('.page-notice.self-hide').addClass('fadeout');
}
