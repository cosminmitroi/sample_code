/*
 *
 * @author: Cosmin Mitroi
 */
app.controller('usersTable', function ($scope, $modal, $filter, $q, $timeout, ngTableParams, request, Status) {
    $scope.active = Status;
    $scope.roles = null;
    $scope.errors = false;

    $scope.usersList = new ngTableParams({
        page: 1,            // show first page
        count: 10,           // count per page
        sorting: {
            id: 'desc'     // initial sorting
        }
    }, {
        counts: [10, 20, 50], // hides page sizes
        getData: function ($defer, params) {
            var filterUsers = {
                'offset': (params.page() - 1) * params.count(),
                'limit': params.count()
            };
            angular.extend(filterUsers, params.filter());

            request.call('UsersService.getAllUsers', filterUsers, function (response) {
                if (response.success) {
                    var orderedData = params.sorting() ? $filter('orderBy')(response.data, params.orderBy()) : response.data;
                    params.total(orderedData.length); // set total for recalculate pagination
                    $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));

                    digest($scope);
                }
            });

        }
    });


    /**
     * Insert/Update Users
     *
     * @param item
     */
    $scope.addEditUserAccount = function (item) {
        $scope.updateSuccess = false;

        // Check if it's update/insert
        if (typeof item !== 'undefined') {
            $scope.userData = item;
        } else {
            $scope.userData = {
                'username': null,
                'email': null,
                'role': 'user',
                'active': 1
            }
        }

        var modalInstance = $modal.open({
            templateUrl: 'addEditUserAccountModal',
            scope: $scope,
            controller: modalAddEditUser
        });

        modalInstance.result.then(function () {
            $scope.usersList.reload();
        }, function () {
            $scope.usersList.reload();
        });

    };


    var modalAddEditUser = function ($scope, $modalInstance, errorsHandler) {
        $scope.errors = false;
        $scope.closeModal = function () {
            $scope.updateSuccess = false;
            errorsHandler.removeErrors($('#addEditUserAccountForm'));
            $modalInstance.dismiss('cancel');
        };

        /**
         * Insert/update an user account
         *
         * @param changed
         */
        $scope.insertUpdateUserAccount = function (validate) {
            var addEditForm = $('#addEditUserAccountForm');

            if (typeof validate !== "undefined" && !validate) {
                return;
            }

            $scope.updateSuccess = false;
            errorsHandler.removeErrors(addEditForm);

            var params = clearEmptyValues($scope.userData);

            if ($('#strength').attr('data-force') !== 'undefined' && $('#strength').attr('data-force') < 5) {
                var errorPasswordMessage = {
                    password: $scope.PasswordErrorMessage
                }

                errorsHandler.handle(addEditForm, errorPasswordMessage);
                return false;
            }

            if (typeof params.$$hashKey !== "undefined") {
                delete params['$$hashKey'];
            }


            request.call('UsersService.saveUsers', [params], function (response) {
                if (response.success) {
                    $scope.updateSuccess = true;
                    $timeout(function () {
                        $modalInstance.dismiss('cancel');
                    }, 3000);
                } else {
                    $scope.errors = response.data;
                    errorsHandler.handle(addEditForm, response.data);
                }

                digest($scope);
            });
        };

    };

    /**
     * Delete Users Account
     *
     * @param item
     */

    $scope.deleteUsers = function (userId) {
        if (typeof userId !== 'undefined') {
            request.call('UsersService.deleteUsers', {'userId': userId}, function (response) {
                if (response.success) {
                    $scope.usersList.reload();
                } else {
                    console.log(response.message);
                }

                digest($scope);
            });
        }
    }

});