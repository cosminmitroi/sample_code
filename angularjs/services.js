/**
 *
 * @author: Cosmin Mitroi
 */
app.service('errorsHandler', function () {
    /**
     * Handles incoming errors in a given form
     *
     * @param form              DOM element
     * @param errors            form_element_name: "Error" array
     */
    this.handle = function (form, errors) {

        if (typeof(errors) === "undefined" || errors === null || errors.length === 0) {
            return;
        }

        // Remove existing errors
        this.removeErrors(form);

        // Add errors to fields
        $.each(errors, function (key, error) {

            var formElement = form.find("[name='" + key + "']");

            // create errors container
            var ul = $('<span  class="help-block"></span>');

            if (typeof error === 'object') {
                $.each(error, function (index, value) {
                    ul.append($('<p>' + value + '</p>'));
                });
            } else {
                ul.append($('<p>' + error + '</p>'));
            }

            // check for element with given name
            if (formElement.length) {
                var parent = formElement.closest('.form-group');

                if (parent && parent.length != 0) {
                    formElement.closest('.form-group').addClass('has-error').append(ul);
                } else {
                    formElement.after(ul);
                }

            }

        });
    };

    /**
     * Remove errors from a given form
     *
     * @param form DOM element
     */
    this.removeErrors = function (form) {
        // remove all errors
        form.find('.has-error').removeClass('has-error');
        form.find('.help-block').remove();
    };

    return this;
});